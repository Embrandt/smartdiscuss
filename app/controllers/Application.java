package controllers;

import play.*;
import play.data.*;
import play.mvc.*;
import models.*;
import play.libs.*;
import play.libs.F.*;
import akka.actor.*;
import java.util.*;

import views.html.*;

public class Application extends Controller {

	final static ActorRef watcher = Watcher.instance;

    public static Result index() {
        return ok("Hello World.");
    }
    
    public static Result moderate() {
        String moderator = session("name");
        if (moderator != null) {
        	createLog("Starte Moderation");
        	return ok( modoverview.render(Discussion.getAll( Long.parseLong(session("userid")) ) ));
        } else {
        	return ok(views.html.index.render("Namen eingeben", 0L));
        }
    }
    
    public static void createLog(String function) {
    	Participant p = null;
    	Discussion d = null;
    	if(session("userid") != null) {
    		Long userid = Long.parseLong(session("userid"));
    		p = Participant.find.ref(userid);
    	} 
    	if(session("discussionid") != null) {
    		Long discussionid = Long.parseLong(session("discussionid"));
    		d = Discussion.find.ref(discussionid);
    	}
    	Log.create(new Log(null, function, p));
    }
    
    public static Result newDiscussion() {
    	DynamicForm filledForm = Form.form().bindFromRequest();
        Discussion.create( new Discussion( filledForm.get("title") , Long.parseLong(session("userid")) ) );
        createLog("Erstelle neue Diskussion");
        return redirect( routes.Application.moderate());
    }
    
    public static Result showDiscussion(long discussion) {
    	Participant self = Participant.find.ref(Long.parseLong(session("userid")));
    	self.setDiscussion(discussion);
    	session( "discussionid", Long.toString(discussion) );
    	self.update();
    	createLog("Zeige Diskussion");
    	return ok( moderate.render(Message.find.where().eq("discussionid", discussion).orderBy("priority asc").findList(), discussion));
    }
    
    public static Result changePriority(String state, long messageid) {
    	Message message1 = Message.find.ref(messageid);
		int priority = message1.priority;
		long discussionid = message1.discussionid;
		Message message2;
    	if(state.equals("up")) {
    		message2 = Message.find.where().eq("discussionid", discussionid).eq("priority", (priority - 1)).findUnique();
    		message1.priority--; 
    		message2.priority++;
    		message2.update();
    		createLog("erhoehePrio");
    	}
    	if (state.equals("down")) {
    		message2 = Message.find.where().eq("discussionid", discussionid).eq("priority", (priority + 1)).findUnique();
    		message1.priority++;    		
    		message2.priority--;
    		message2.update();
    		createLog("senkePrio");
    	}
    	
    	if (state.equals("talk")) {
    		Message talker = Message.find.where().eq("discussionid", discussionid).eq("priority", 0).findUnique();
    		if (talker != null) {
    			Watcher.instance.tell(talker.id, null);
    		}
        	List<Message> mList = Message.find.where().eq("discussionid", discussionid).gt("priority", priority).findList();
        	for(Message m : mList) {
        		m.priority--;
        		m.update();
        	}
    		message1.priority = 0;
    		Watcher.instance.tell("giveTalktime:"+message1.id, null);
    		createLog("gebeSprechzeit");
    	}
    	
    	if(state.equals("end")) {
    		watcher.tell(messageid, null);
    		createLog("entzieheSprechzeit");
    	}
    	
   		message1.update();
    	return redirect(routes.Application.showDiscussion(discussionid));
    }
    
    public static Result setDiscussionTitle(String title) {
    	Long discussionid = Long.parseLong(session("discussionid"));
    	Discussion discussion = Discussion.find.ref(discussionid);
    	discussion.title = title;
    	discussion.update();
    	watcher.tell("newTitle:"+title, null);
    	createLog("Setze Thema");
    	return ok(title);
    }
    
    public static Result discuss(long discussion) {
        String participant = session("name");
        if (participant != null) {
        	Participant self = Participant.find.ref(Long.parseLong(session("userid")));
        	self.setDiscussion(discussion);
        	session( "discussionid", Long.toString(discussion) );
        	self.update();
        	createLog("Starte Teilnahme");
        	return ok( views.html.discussion.render( Discussion.find.ref(discussion) ) );
        } else {
        	return ok(views.html.index.render("Namen eingeben", discussion));
        }
    }

    public static Result messages() {
    	long discussionid = Long.parseLong(session("discussionid"));
        return ok( views.html.discussion.render(Discussion.find.ref(discussionid)));
    }
    
    public static Result newMessage(String type) {
        Message myMessage =  new Message(Long.parseLong(session("userid")), type);
        Message.create( myMessage );
        myMessage.refresh();
        session("messageid", Long.toString(myMessage.id));
        Watcher.instance.tell("newMessage", null);
        createLog("Erfrage Sprechzeit");
        return redirect(routes.Application.messages());
    }
    
    public static Result choseName(long discussion) {
        DynamicForm nameForm = Form.form().bindFromRequest();	
        session("name", nameForm.get("name"));
        
        	Participant self = new Participant( session("name"), discussion);
        	Participant.create( self );
        	self.refresh();
        	session( "userid", Long.toString( self.getId() ) );
        	session( "discussionid", Long.toString(discussion) );
        	createLog("Erstelle Namen");
        if (discussion != 0) {
        	return redirect(routes.Application.messages());
        } else {
        	return redirect(routes.Application.moderate());
        }
    }
    
    
    public static Result deleteMessage(Long id) {
    	Message message = Message.find.ref(id);
		int priority = message.priority;
		long discussionid = message.discussionid;
    	if(message.priority > 0) {
		List<Message> mList = Message.find.where().eq("discussionid", discussionid).gt("priority", priority).findList();
    	for(Message m : mList) {
    		m.priority--;
    		m.update();
    		
    	}
    	}
    	Message.delete(id);
    	session().remove("messageid");
    	Watcher.instance.tell("newMessage", null);
    	createLog("Ziehe Anfrage zurueck");
    	return redirect(routes.Application.messages());
    }
    public static Result unsetSession() {
    	session().remove("messageid");
    	return redirect(routes.Application.messages());
    }
    
    public static Result listMessages(Long discussion) {
    	return ok(test.render(Message.find.where().eq("discussionid", discussion).orderBy("priority asc").findList()));
    }
    
    public static Result javascriptRoutes() {
    	response().setContentType("text/javascript");
    	return ok(
    			Routes.javascriptRouter("jsRoutes",
    					controllers.routes.javascript.Application.deleteMessage(),
    					controllers.routes.javascript.Application.messages(),
    					controllers.routes.javascript.Application.setDiscussionTitle(),
    					controllers.routes.javascript.Application.listMessages()
    			)
    	);
    }
    
    
    public static Result watchMessages() {
    	return ok(new EventSource() {
    		public void onConnected() {
    			watcher.tell(this, null);
    		}
    	});
    }
    
    
    public static class Watcher extends UntypedActor {
    	public static ActorRef instance = Akka.system().actorOf(Props.create(Watcher.class));
    	List<EventSource> sockets = new ArrayList<EventSource>();
    	public void onReceive(Object message) {
    		//TODO
            if(message instanceof EventSource) {
                final EventSource eventSource = (EventSource)message;
                
                if(sockets.contains(eventSource)) {                    

                    // Browser is disconnected
                    if(sockets.contains(eventSource)){
                    	sockets.remove(eventSource);
                    }
                    Logger.info("Browser disconnected (" + sockets.size() + " browsers currently connected)");
                   
                    
                } else {                    
                    // Register disconnected callback 
                    eventSource.onDisconnected(new Callback0() {
                        public void invoke() {
                            getContext().self().tell(eventSource, null);

                        }
                    });                    
                    // New browser connected
                    sockets.add(eventSource);
                    Logger.info("New browser connected (" + sockets.size() + " browsers currently connected)");
                    
                }
                
            }
            if(message instanceof String) {
            	
                // Send updates
                List<EventSource> shallowCopy = new ArrayList<EventSource>(sockets); //prevent ConcurrentModificationException
                for(EventSource es: shallowCopy) {
                	if("newMessage".equals(message)) {
                		es.sendData( "newMessage" );
                	}
                	if(((String) message).startsWith("giveTalktime:")) {
                		es.sendData( (String) message );
                	}
                	if(((String) message).startsWith("newTitle:")) {
                		es.sendData( (String) message );
                	}
                }      
            }
            
            if(message instanceof Long) {

                List<EventSource> shallowCopy = new ArrayList<EventSource>(sockets); //prevent ConcurrentModificationException
                for(EventSource es: shallowCopy) {
                	es.sendData( message.toString() );
                }      
            }
    	}
    }

}
