package models;

import java.util.*;
import javax.persistence.*;
import play.db.ebean.*;
import play.db.ebean.Model.Finder;



@Entity
public class Discussion extends Model{

	@Id
	public Long id;
	public String title;
	public long moderatorid;
	
	public Discussion(String title, long moderatorid) {
		this.title = title;
		this.moderatorid = moderatorid;
	}
	
	public String getTitle() {
		return this.title;
	}
	public Long getId() {
		return this.id;
	}
	
	public static Finder<Long,Discussion> find = new Finder<Long, Discussion>( Long.class, Discussion.class);
	public static List<Discussion> getAll(long moderatorid) {
		return find.where().eq("moderatorid", moderatorid).findList();
	}
	
	public static void create( Discussion discussion) {
		discussion.save();
	}
}