package models;

import java.util.*;
import play.db.ebean.*;
import javax.persistence.*;

@Entity
public class Message extends Model{

	@OneToOne
	public Participant participant;
	public long discussionid;
	public String state;
	public Date time;
	public int priority;
	@Id
	public long id;


	public Message(long participantid, String state) {
        this.participant = Participant.find.ref(participantid);
        this.discussionid = participant.discussion.getId();
        this.state = state;
        this.time = new Date();        
        List<Message>  messages = find.fetch("participant").where().eq("participant.discussion.id", discussionid).orderBy("priority asc").findList();
        int maxPriority = 0;
        if(messages != null & messages.size() > 0) {
        	maxPriority = messages.get(messages.size()-1).priority;
        }
        this.priority = maxPriority + 1;
    }

  public static Finder<Long,Message> find = new Finder<Long, Message>( Long.class, Message.class);
  public static List<Message> getAll() {
    return find.all();
  }
  public static Message getByUserId(Long userid) {
	    return find.where().eq("participant.id", userid).findUnique();
  }

  public static void create(Message message) {
	message.save();
  }

  public static void delete (long id) {
      find.ref(id).delete();
  }
}
