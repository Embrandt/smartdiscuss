package models;

import javax.persistence.*;
import play.db.ebean.*;
import java.util.*;

@Entity
public class Log extends Model{

	@Id
	private Long id;
	@ManyToOne
	private Discussion discussion;
	private String function;
	@OneToOne
	private Participant participant;
	private Date time;
	
	public Log(Discussion discussion, String function, Participant participant) {
		this.discussion = discussion;
		this.function = function;
		this.participant = participant;
		this.time = new Date( );
	}
	
	public static Finder<Long,Discussion> find = new Finder( Long.class, Discussion.class);
	
	public static void create (Log log) {
		log.save();
	}
}