package models;

import javax.persistence.*;
import play.db.ebean.*;

@Entity
public class Participant extends Model{

	@Id
	public long id;
	public String name;
	@ManyToOne
	public Discussion discussion;
	private int participation;
	
	public Participant(String name, long discussion) {
		this.name = name;
		if(discussion != 0) {
			this.discussion = Discussion.find.ref(discussion);
		}
		participation = 0;
	}
	
	public long getId() {
		return id;
	}
	public void setDiscussion(long discussion) {
		this.discussion = Discussion.find.ref(discussion);
	}
	
	public static Finder<Long,Participant> find = new Finder( Long.class, Participant.class);
	
	public static void create ( Participant participant ) {
		participant.save();
	}
}
