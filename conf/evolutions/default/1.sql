# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table discussion (
  id                        bigint not null,
  title                     varchar(255),
  moderatorid               bigint,
  constraint pk_discussion primary key (id))
;

create table log (
  id                        bigint not null,
  discussion_id             bigint,
  function                  varchar(255),
  participant_id            bigint,
  time                      timestamp,
  constraint pk_log primary key (id))
;

create table message (
  id                        bigint not null,
  participant_id            bigint,
  discussionid              bigint,
  state                     varchar(255),
  time                      timestamp,
  priority                  integer,
  constraint pk_message primary key (id))
;

create table participant (
  id                        bigint not null,
  name                      varchar(255),
  discussion_id             bigint,
  participation             integer,
  constraint pk_participant primary key (id))
;

create sequence discussion_seq;

create sequence log_seq;

create sequence message_seq;

create sequence participant_seq;

alter table log add constraint fk_log_discussion_1 foreign key (discussion_id) references discussion (id) on delete restrict on update restrict;
create index ix_log_discussion_1 on log (discussion_id);
alter table log add constraint fk_log_participant_2 foreign key (participant_id) references participant (id) on delete restrict on update restrict;
create index ix_log_participant_2 on log (participant_id);
alter table message add constraint fk_message_participant_3 foreign key (participant_id) references participant (id) on delete restrict on update restrict;
create index ix_message_participant_3 on message (participant_id);
alter table participant add constraint fk_participant_discussion_4 foreign key (discussion_id) references discussion (id) on delete restrict on update restrict;
create index ix_participant_discussion_4 on participant (discussion_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists discussion;

drop table if exists log;

drop table if exists message;

drop table if exists participant;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists discussion_seq;

drop sequence if exists log_seq;

drop sequence if exists message_seq;

drop sequence if exists participant_seq;

