package models;

import models.*;
import org.junit.*;
import static org.junit.Assert.*;
import play.test.WithApplication;
import static play.test.Helpers.*;

public class ModelsTest extends WithApplication {
    @Before
    public void setUp() {
        start(fakeApplication(inMemoryDatabase()));
    }
    
    @Test
    public void createAndRetrieveParticipant() {
    	new Participant("John Doe", 1).save();
    	Participant john = Participant.find.where().eq("name", "John Doe").findUnique();
    	assertNotNull(john);
    	assertEquals("John Doe", john.name);
    }
}